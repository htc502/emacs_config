(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files nil)
 '(package-archives (quote (("org" . "http://orgmode.org/elpa/") ("gnu" . "http://elpa.gnu.org/packages/") ("marmalade" . "http://marmalade-repo.org/packages/") ("melpa" . "http://melpa.milkbox.net/packages/"))))
 '(show-paren-mode t)
 '(text-mode-hook (quote (turn-on-auto-fill text-mode-hook-identify)))
 '(tool-bar-mode nil)
 '(transient-mark-mode (quote (only . t))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "΢���ź�" :foundry "outline" :slant normal :weight normal :height 120 :width normal)))))


;;evil settings
;;evil require undo-tree
(add-to-list 'load-path "C:/emacs24/.emacs.d/elpa/undo-tree-20140509.522")
(require 'undo-tree)
(add-to-list 'load-path "C:/emacs24/.emacs.d/elpa/evil-20140609.2317")
(require 'evil)
(evil-mode 1)

;;color theme
(add-to-list 'load-path "C:/emacs24/.emacs.d/elpa/color-theme-20080305.34")
(add-to-list 'load-path "C:/emacs24/.emacs.d/elpa/color-theme-20080305.34/themes")
(require 'color-theme)
(color-theme-initialize)
(setq color-theme-is-goabal t)
(add-to-list 'load-path "C:/emacs24/.emacs.d/elpa/color-theme-solarized-20140408.1309")
(require 'color-theme-solarized)
(load-theme 'solarized-dark t)

;;ess-make emacs to interact with R
(add-to-list 'load-path "C:/emacs24/.emacs.d/elpa/ess-20140522.945/lisp")
(load "ess-site")

;;smex settings
(add-to-list 'load-path "C:/emacs24/.emacs.d/elpa/smex-20140425.1314")
(require 'smex)
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
;; This is your old M-x.
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

;;;org-mode settings
(add-to-list 'load-path "C:/emacs24/.emacs.d/elpa/org-8.2.6/lisp")
(require 'org)
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-log-done t)
(setq org-agenda-files (list "C:/Users/ewre/Desktop/org-mode/work.org"
			   "C:/Users/ewre/Desktop/org-mode/home.org"))
;;spell checker
(add-to-list 'exec-path "C:/Aspell/bin/")
(setq ispell-program-name "C:/Aspell/bin/aspell.exe")
(setq ispell-personal-dictionary "C:/Aspell/dict")
(require 'ispell)

;;tramp edit file over ssh connection
(setq tramp-default-method "ssh")

;;google translate
(add-to-list 'load-path "C:/emacs24/.emacs.d/elpa/google-translate-20140524.434")
(require 'google-translate)
(require 'google-translate-default-ui)

;;markdown mode
(autoload 'markdown-mode "markdown-mode" 
  "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.text\\'" . markdown-mode) )
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode) )
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode) )
